#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import gzip
import json
import logging
import os
import pickle


from abc import ABCMeta, abstractmethod


class BaseIndexData(metaclass=ABCMeta):
    """
    Base index data structure using python dictionary
    """

    def __init__(self):

        # Main data object
        self.data_dict = {}

        # File name for save/load, **MUST** override in child classes
        self.file_name_json = ""
        self.file_name_pickle = ""

    def __str__(self):
        return json.dumps(self.data_dict, sort_keys=True, indent=4,
                          separators=(',', ': '), ensure_ascii=False)

    def dump_json(self, json_file_path):

        # Create subdir(index_name) in given path
        if not os.path.exists(json_file_path):
            os.makedirs(json_file_path, 0o755)

        with open(os.path.join(json_file_path, self.file_name_json), 'w') as f:
            json.dump(self.data_dict, f, ensure_ascii=False, separators=(',', ':'))

    def load_json(self, json_file_path):

        full_path = os.path.join(json_file_path, self.file_name_json)

        if os.path.exists(full_path):
            # Clear existing data
            self.data_dict = {}

            # Load!
            with open(full_path, 'r') as f:
                self.data_dict = json.load(f)
                logging.info("{0} loaded.".format(self.file_name_json))
        else:
            raise FileNotFoundError("Could not load index files!")

    def dump_pickle(self, pickle_file_path):

        # Create subdir(index_name) in given path
        if not os.path.exists(pickle_file_path):
            os.makedirs(pickle_file_path, 0o755)

        with gzip.open(os.path.join(pickle_file_path, self.file_name_pickle), 'wb') as f:
            pickle.dump(self.data_dict, f)

    def load_pickle(self, pickle_file_path):

        full_path = os.path.join(pickle_file_path, self.file_name_pickle)

        if os.path.exists(full_path):
            # Clear existing data
            self.data_dict = {}

            # Load!
            with gzip.open(full_path, 'rb') as f:
                self.data_dict = pickle.load(f)
                logging.info("{0} loaded.".format(self.file_name_json))
        else:
            raise FileNotFoundError("Could not load index files!")

    @abstractmethod
    def add_document(self, doc_id, field, term_list):
        pass
