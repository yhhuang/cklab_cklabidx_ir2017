#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

from cklabidx.index.data.base_index_data import BaseIndexData


class DocValues(BaseIndexData):
    """
    Data structure for Inverted Table, storage by documents.

    Object format for data_dict:
    {
        "doc_id": {
            "field1": {
                "term1": counts,
                "term2": counts,
                "term3": counts,
            },
            "field2": {
                "term4": counts,
                "term5": counts,
                "term6": counts,
            },
            ...
        }
    }
    """
    def __init__(self):
        super().__init__()

        # Override
        self.data_dict = {}
        self.file_name_json = "it_doc_values.json"
        self.file_name_pickle = "it_doc_values.pickle"

    def add_document(self, doc_id, field, term_list):

        # Initialize document and fields
        if doc_id not in self.data_dict:
            self.data_dict[doc_id] = {}
        if field not in self.data_dict[doc_id]:
            self.data_dict[doc_id][field] = {}

        # Add terms to document field
        for term in term_list:
            if term not in self.data_dict[doc_id][field]:
                self.data_dict[doc_id][field][term] = 1
            else:
                self.data_dict[doc_id][field][term] += 1

    def get_term_counts(self, doc, field):
        return len(self.data_dict[doc][field])

    def get_document_counts(self):
        return len(self.data_dict)

    def get_document_vector(self, doc, field):
        if doc in self.data_dict and field in self.data_dict[doc]:
            return self.data_dict[doc][field]
        else:
            return {}


class TermValues(BaseIndexData):
    """
    Data structure for Inverted Table, storage by fields.

    Object format for data_dict:
    {
        "field_name": {
            "term1": {
                 "doc_id1": counts,
                 "doc_id2": counts,
                ...
            },
            "term2": {
                 "doc_id2": counts,
                 "doc_id3": counts,
                ...
            },
            ...
        }
    }
    """

    def __init__(self):
        super().__init__()

        # Override
        self.data_dict = {}
        self.file_name_json = "it_term_values.json"
        self.file_name_pickle = "it_term_values.pickle"

    def add_document(self, doc_id, field, term_list):

        # Initialize field and terms
        if field not in self.data_dict:
            self.data_dict[field] = {}

        for term in term_list:
            if term not in self.data_dict[field]:
                self.data_dict[field][term] = {}
            # Add doc_id to field term
            if doc_id not in self.data_dict[field][term]:
                self.data_dict[field][term][doc_id] = 1
            else:
                self.data_dict[field][term][doc_id] += 1

    def get_field_counts(self):
        return len(self.data_dict)

    def get_term_counts(self, field):
        return len(self.data_dict[field])
