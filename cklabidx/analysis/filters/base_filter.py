#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

from abc import ABCMeta, abstractmethod


class Filter(metaclass=ABCMeta):
    """
    Base Filter
    """

    def __init__(self):
        pass

    @staticmethod
    @abstractmethod
    def filter(token_list):
        """
        Null Filter
        :param token_list: List of Tokens
        :return: Original token list
        """
        return token_list
