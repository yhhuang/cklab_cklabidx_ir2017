#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

from cklabidx.analysis.filters.lower_case_filter import LowerCaseFilter
from cklabidx.analysis.processors.en_punctuation_processor import PunctuationProcessor as EN_PP
from cklabidx.analysis.tokenizers.whitespace_tokenizer import WhitespaceTokenizer


class Analyzer:
    """
    Analyzer Object Formats:

        defaults:
            Provide default tokenizer and filters objects.
        fields:
            Set specific tokenizer and filter to the certain field.

    """
    def __init__(self):
        self.defaults = {
            "tokenizer": WhitespaceTokenizer(),
            "filters": [LowerCaseFilter()],
            "preprocessors": [EN_PP()]
        }
        self.fields = {}

    def set_default(self, tokenizer, filter_list, preprocessor_list):
        self.defaults = {
            "tokenizer": tokenizer,
            "filters": filter_list,
            "preprocessors": preprocessor_list
        }

    def add_field(self, field_name, tokenizer, filter_list, preprocessor_list):
        self.fields[field_name] = {
            "tokenizer": tokenizer,
            "filters": filter_list,
            "preprocessors": preprocessor_list
        }

    def analyze(self, field, content):
        """
        Return a list of processed tokens
        :param field:
        :param content:
        :return:
        """
        # Apply Preprocessor
        content = self.apply_preprocessors(field, content)
        # Apply Tokenizer
        tokens = self.apply_tokenizer(field, content)
        # Apply Filters
        tokens = self.apply_token_filters(field, tokens)
        return tokens

    def apply_preprocessors(self, field, content):
        """
        Apply Preprocessor
        :param field:
        :param content:
        :return:
        """
        # Apply Preprocessor
        for preprocessor in self.get_preprocessors(field):
            content = preprocessor.process(content)

        return content

    def apply_tokenizer(self, field, content):
        """
        Apply tokenizer
        :param field:
        :param content:
        :return:
        """
        # Apply Tokenizer
        tokenizer = self.get_tokenizer(field)
        tokens = tokenizer.generate_tokens(content)

        return tokens

    def apply_token_filters(self, field, tokens):
        """
        Apply filters
        :param field:
        :param tokens:
        :return:
        """
        # Apply Filters
        for token_filter in self.get_filters(field):
            tokens = token_filter.filter(tokens)

        return tokens

    def get_tokenizer(self, field_name):
        if field_name in self.fields:
            return self.fields[field_name]["tokenizer"]
        else:
            return self.defaults["tokenizer"]

    def get_filters(self, field_name):
        if field_name in self.fields:
            return self.fields[field_name]["filters"]
        else:
            return self.defaults["filters"]

    def get_preprocessors(self, field_name):
        if field_name in self.fields:
            return self.fields[field_name]["preprocessors"]
        else:
            return self.defaults["preprocessors"]
