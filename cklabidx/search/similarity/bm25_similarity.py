#! /usr/bin/env python3

# Open Source CKLab Index Library
#
# Simplified version for IR Courses, 2017
# Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE
#

import math

from cklabidx.search.similarity.base_similarity import Similarity


class BM25Similarity(Similarity):
    """
    Document similarity scores using BM25
    Extends Scores Class in base_scorer.py
    """

    def __init__(self, use_explain=False, k1=1.2, b=0.75):
        super().__init__(use_explain)

        if math.isfinite(k1) is False or k1 < 0:
            raise ValueError("illegal k1 value: " + str(k1) + ", must be a non-negative finite value")

        if math.isnan(b) or b < 0 or b > 1:
            raise ValueError("illegal b value: " + str(b) + ", must be between 0 and 1")

        self.k1 = k1
        self.b = b

        if self.use_explain:
            self.explain.notation = "SUM[ idf · ( tf · (k_1 + 1) / ( tf + k_1 · ( 1 - b + b · ( |d| / avgdl ) ) ) ]"

    def update_hit_doc_list(self, field, term, hit_doc_list):
        """
        Call once for each term in query fields
        :param field:
        :param term:
        :param hit_doc_list:
        :return:
        """
        pass

    def _get_avg_field_length(self, field):
        sum_total_term_freq = self.index_object.get_sum_total_term_counts(field)
        doc_count = self.index_object.get_unique_doc_counts()
        return sum_total_term_freq / doc_count

    def _get_term_idf_lucene_bm25(self, field, term):
        """
        Get Term Inverted Document Frequency
            for term(i),
                total document counts(doc_count),
                total document counts contains term(i) (doc_freq):

            log(1 + (doc_count - doc_freq + 0.5)/(doc_freq + 0.5))
        https://lucene.apache.org/core/6_1_0/core/org/apache/lucene/search/similarities/BM25Similarity.html
        :param field: field
        :param term: term
        :return: inverted document frequency
        """
        doc_count = self.index_object.get_unique_doc_counts()
        doc_freq = self.index_object.get_doc_counts_contains_term(field, term)

        # Short circuit when td == 0
        if doc_freq == 0:
            return 0

        return math.log(1 + (doc_count - doc_freq + 0.5) / (doc_freq + 0.5), 10)

    def get_scores(self, field, term_list, hit_doc_list):
        """
        Implements Lucene BM25Similarity
        (Ref: https://lucene.apache.org/core/6_1_0/core/org/apache/lucene/search/similarities/BM25Similarity.html
              http://ipl.cs.aueb.gr/stougiannis/bm25.html

        Practical Scoring Function

        score(q,d) =
                     |q|
                      ∑  idf(q_i) · ( tf(q_i,d) · (k_1 + 1) / tf(q_i,d) + k_1 · (1 - b + b · ( |d| / avgdl ) )
                     i=1

        f(qi,d) correlates to the term's frequency, defined as the number of times query term q_i appears in the
        document d

        | d | is the length of the document d in words (terms). In our implementation |d| is defined by:
        | d | = 1/(norm*norm) , where norm is the score factor used by Lucene's default similarity function.

        avgdl is the average document length over all the documents of the collection.

        k1 and b are free parameters, usually chosen as k1 = 2.0 and b = 0.75.

        idf(qi) is the inverse document frequency weight of the query term qi

        :param field:
        :param term_list:
        :param hit_doc_list:
        :return:
        """
        doc_scores_dict = {}

        for doc in hit_doc_list:

            # 1. Iterate over all terms to get SUM of BM25
            sum_bm25_score = 0
            sum_bm25_explain_list = []    # Formula string

            # 2. get average document length(avgdl)
            avgdl = self._get_avg_field_length(field)

            for term in term_list:

                # 3. term's frequency
                tf = self.index_object.get_term_tf(doc, field, term)

                # 4. IDF
                idf = self._get_term_idf_lucene_bm25(field, term)

                # 5. norm_value(|d|) is the length of the document d in words (terms).
                #    |d| is defined by: | d | = 1/(norm*norm)
                #    norm(t,d)
                #    norm(t,d) = lengthNorm ·         ∏                f.boost()
                #                             field f in d named as t
                field_boost = 1  # No implement, set to 1
                length_norm = self.index_object.get_length_norm_of_doc(doc, field)
                norm = length_norm * field_boost
                norm_value = 1 / norm ** 2

                # bm25_norm: compute freq-independent part of bm25 equation
                bm25_norm = self.k1 * (1 - self.b + (self.b * (norm_value / avgdl)))

                # SUM of BM25 score
                sum_bm25_score += idf * (tf * (self.k1 + 1) / (tf + bm25_norm))

                # Store scoring explain
                if self.use_explain:
                    sum_bm25_explain_list.append("({idf} · ( {tf} · ({k_1} + 1) / ( {tf} + {k_1} · (1 - {b} + {b} · "
                                                 "( {d} / {avgdl} ) ) )"
                                                 .format(idf=idf,
                                                         tf=tf,
                                                         k_1=self.k1,
                                                         b=self.b,
                                                         d=norm_value,
                                                         avgdl=avgdl)
                                                 )

            # The final score!
            doc_score = sum_bm25_score

            doc_scores_dict[doc] = doc_score

            # Store scoring explain
            if self.use_explain:
                sum_bm25_explain_str = " + ".join(sum_bm25_explain_list)
                explain = "[ {sum_bm25} ]".format(
                    sum_bm25=sum_bm25_explain_str
                )

                self.explain.add_document(doc, explain, doc_score)

        return doc_scores_dict
