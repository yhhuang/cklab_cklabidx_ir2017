# CKLabidx for IR Course 2017

The skeleton code for IR Course, 2017. A simplified version of the open source CKLab Index Library.

Copyright (c) 2017, Computational Knowledge Lab @ NTU ESOE

## Requirements

* Python 3

## Exercises

### Analyzer / NLP
* Compound tokenizer
* Processor to remove all Chinese punctuation 
* Stemming filter

### Index Functions

* TF function
* IDF function (Classic/Lucene default)


## Projects

### IR Models

* BM25 similarity
* Language Model similarity

### Index Storage

* Index storage using database (SQLite/MySQL/PostgreSQL)
* Suffix Array Index

### Performance Issues

* Using numpy/pandas to improve search performance


## Unit Testing

Run all test cases: 
~~~
$ cd <Project root>
$ python -m unittest discover tests/
~~~
